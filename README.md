# Twitter poster (API) and VK image grabber #

Пример работы с Twitter API и парсинга картинок из группы VK библиотекой Pyrhon requests

### Ключевые технологии ###

* [Beautifulsoup4 4.6.0] (https://pypi.python.org/pypi/beautifulsoup4)
* [Python Requests library 2.18.4] (http://docs.python-requests.org/en/master/)
* [Twitter API documentation] (https://developer.twitter.com/en/docs)

### Контакты ###

* rvk.sft[ at ]gmail.com