import os
import tweepy
import re
from settings import ACCESS_TOKEN, ACCESS_TOKEN_SECRET, API_KEY, API_SECRET
import requests
import random
from bs4 import BeautifulSoup


def twitter_auth():
    auth = tweepy.OAuthHandler(API_KEY, API_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth)
    return api


def get_post():
    length = 141
    while length >= 115:
        quote_url = 'http://bash.im/random'
        req = requests.get(quote_url)
        soup = BeautifulSoup(req.text, 'html.parser')
        quote = random.choice(soup.find_all('div', class_='text'))
        quote = re.sub(r'<[a-z\s=]*=[a-z"]*>', ' ', str(quote))
        quote = re.sub(r'<\/div>', ' ', quote)
        quote = re.sub(r'<br\/>', '\n', quote)
        quote = re.sub(r'<\/br>', '\n', quote)
        quote = re.sub(r'<br>', '\n', quote)
        length = len(quote)
    return quote+'\n#живикрасиво #навальный'

def get_media():
    files = os.listdir('images/')
    return 'images/' + random.choice(files)


def rand_tweet_w_media(api):
    post = get_post()
    media = get_media()
    api.update_with_media(media, post)

def rand_tweet_wo_media(api):
    post = get_post()
    api.update_status(post)

def main():
    api = twitter_auth()
    # api.update_profile_image(get_media())
    # rand_tweet_wo_media(api)
    rand_tweet_w_media(api)



if __name__=='__main__':
    main()