import requests
from bs4 import BeautifulSoup
import re
import os
from time import sleep

def get_html(url):
    r = requests.get(url)
    return r.text


def input_info():
    wall_adress = 'https://m.vk.com/foto_pazl'
    deep = 10000
    return wall_adress, deep


def get_img(url):
    sleep(1)
    r = requests.get(url, stream=True)
    return r


def get_name(url):
    name = url.split('/')[-1]
    folder = "images/"
    if not os.path.exists(folder):
        os.makedirs(folder)
    path = os.path.abspath(folder)
    return path + "/" + name


def save_image(name, fileObject):
    try:
        with open(name, "bw") as f:
            for chunk in fileObject.iter_content(8192):
                f.write(chunk)
    except Exception as e:
        print(e)
def find_post(html):
    soup = BeautifulSoup(html, 'html.parser')
    posts = soup.find_all("div", class_="wall_item")
    for post in posts:
        imgs = post.find_all("div", class_="thumb_map_img")
        for img in imgs:
            img_links = re.findall(r'https:\/\/([a-zA-Z.\/0-9]*)', img.attrs['data-src_big'])
            for img_link in img_links:
                print(img_link)
                save_image(get_name(img_link), get_img('http://' + img_link))


'?offset=1025'

def main():
    url, deep = input_info()

    for i in range(5, int(deep), 10):
        link = url + '?offset=' + str(i) + '&own=1'
        find_post(get_html(link))
        link = ''

if __name__ == '__main__':
    main()
